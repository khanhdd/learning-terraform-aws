# Learn DevOps: Infrastructure Automation With Terraform

## Course
https://www.udemy.com/learn-devops-infrastructure-automation-with-terraform/

## Demo overview
Demo Directory | Description
------------ | -------------
packer-demo | Build AMIs with Packer
jenkins-packer-demo | Demo with jenkins and Packer
docker-demo-1 | Using ECR - The EC2 Container Registry
docker-demo-2 | Using ECS - The EC2 Container Service
docker-demo-3 | Using ECR/ECS with Jenkins in a complete workflow
module-demo | Using ECS + ALB in 4 modules to show how developing terraform modules work

# AWS Certified Solutions Architect - Associate 2019

## Course
https://www.udemy.com/aws-certified-solutions-architect-associate/



