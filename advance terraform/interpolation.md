# Interpolation (nội suy)

## Interpolate other values, using ${...}

* simple math functions
* variables: e.g. ${var.VARIABLE_NAME} refers to a variable
* resources: e.g. ${aws_instance_name.id} (type.resource-name.attr)
* data source: e.g. ${data.template_file.name.rendered} (data.type.resource-name.attr)
* conditionals (if-else)
* functions

## Usage

* variables

```json
    string var:
        ${var.SOMETHING}

    map var:
        ${var.AMIS["ap-southeast-1"]}
        ${lookup(var.AMIS, var.AWS_REGION)}

    list var: 
        ${var.subnets[i]}
        ${join(",", var.subnets)}
```

* various

    * refer to course bookmark
    [interpolation-various](https://www.udemy.com/learn-devops-infrastructure-automation-with-terraform/learn/v4/bookmarks)

* conditionals

```json
    CONDITON ? TRUEVAL : FALSEVAL
    e.g.
        count = "${var.env == "prod" ? 2 : 1}"
```

* functions

    * refer to docs: [interpolation](https://www.terraform.io/docs/configuration-0-11/interpolation.html)
