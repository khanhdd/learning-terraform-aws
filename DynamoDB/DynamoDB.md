# Dynamo DB

* Stored on SSD storage
* Spread across 3 geographically distinct data centres
* Eventual Consistent Reads (Default - Best Read Performance):
    * Application write and won't read that data within the 1st second
* Strongly Consistent Reads 
    * Write and read within 1st second