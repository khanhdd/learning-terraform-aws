variable "AWS_REGION" {
  default = "ap-southeast-1"
}

variable "ENV" {
  default = "dev"
}

variable "AMIS" {
  type = "map"

  default = {
    ap-southeast-1 = "ami-01f7527546b557442"
  }
}

variable "RDS_PASSWORD" {}
