resource "aws_s3_bucket" "wp-code-bucket" {
  bucket = "wpcode-bucket-ddxx"
  acl    = "private"
}

resource "aws_s3_bucket" "wp-media-bucket" {
  bucket = "wpmedia-bucket-ddxx"
  acl    = "private"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicReadGetObject",
            "Effect": "Allow",
            "Principal": "*",
            "Action": [
                "s3:GetObject"
            ],
            "Resource": [
                "arn:aws:s3:::wpmedia-bucket-ddxx/*"
            ]
        }
    ]
}
EOF
}

output "s3_media_bucket" {
  value = "${aws_s3_bucket.wp-media-bucket.id}"
}

output "s3_media_regional_domain" {
  value = "${aws_s3_bucket.wp-media-bucket.bucket_regional_domain_name}"
}
