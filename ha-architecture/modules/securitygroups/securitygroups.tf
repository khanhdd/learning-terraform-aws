variable "VPC_ID" {}

variable "ENV" {}

resource "aws_security_group" "web-dmz" {
  vpc_id      = "${var.VPC_ID}"
  name        = "web-dmz-${var.ENV}"
  description = "security group that allows ssh and all egress traffic"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name         = "web-dmz"
    Terraform    = "true"
    Environmnent = "${var.ENV}"
  }
}

resource "aws_security_group" "rds-launch" {
  vpc_id      = "${var.VPC_ID}"
  name        = "rds-launch-${var.ENV}"
  description = "security group that allows ssh and all egress traffic"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = ["${aws_security_group.web-dmz.id}"]
  }

  tags {
    Name         = "rds-launch"
    Terraform    = "true"
    Environmnent = "${var.ENV}"
  }
}

output "web_dmz_id" {
  value = "${aws_security_group.web-dmz.id}"
}

output "rds_launch_id" {
  value = "${aws_security_group.rds-launch.id}"
}
