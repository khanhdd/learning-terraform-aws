variable "SECURITY_GROUP" {}
variable "ENV" {}
variable "VPC_ID" {}
variable "INSTANCE_ID" {}

variable "PUBLIC_SUBNETS" {
  type = "list"
}

resource "aws_lb" "wp-alb" {
  name               = "wp-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = ["${var.SECURITY_GROUP}"]
  subnets            = ["${var.PUBLIC_SUBNETS}"]

  enable_deletion_protection = true

  #   access_logs {
  #     bucket  = "${aws_s3_bucket.lb_logs.bucket}"
  #     prefix  = "test-lb"
  #     enabled = true
  #   }

  tags = {
    Terraform   = "true"
    Environment = "${var.ENV}"
  }
}

resource "aws_lb_listener" "alb-listener" {
  load_balancer_arn = "${aws_lb.wp-alb.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.wp-target-group.arn}"
  }
}

resource "aws_lb_target_group" "wp-target-group" {
  name     = "wp-target-group"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${var.VPC_ID}"

  health_check = {
    enabled  = true
    protocol = "HTTP"
    path     = "/healthy.html"
    port     = "traffic-port"

    healthy_threshold   = "2"
    unhealthy_threshold = "3"
    timeout             = "5"
    interval            = "6"
    matcher             = "200"
  }

  tags = {
    Terraform   = "true"
    Environment = "${var.ENV}"
  }
}

# resource "aws_lb_target_group_attachment" "wp-target-group-attachment" {
#   target_group_arn = "${aws_lb_target_group.wp-target-group.arn}"
#   target_id        = "${var.INSTANCE_ID}"
#   port             = 80
# }

output "target_group_arn" {
  value = "${aws_lb_target_group.wp-target-group.arn}"
}
