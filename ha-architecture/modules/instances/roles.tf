resource "aws_iam_role" "s3-wpbucket-role" {
  name = "s3-wpbucket-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "s3-wpbucket-role-policy" {
  name = "s3-wpbucket-role-policy"
  role = "${aws_iam_role.s3-wpbucket-role.id}"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
              "s3:*"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_instance_profile" "s3-wpbucket-role-instanceprofile" {
  name = "s3-wpbucket-role"
  role = "${aws_iam_role.s3-wpbucket-role.name}"
}
