resource "aws_instance" "instance" {
  ami           = "${var.AMI}"
  instance_type = "${var.INSTANCE_TYPE}"

  # the VPC subnet
  subnet_id = "${var.PUBLIC_SUBNETS[0]}"

  # the security group
  vpc_security_group_ids = ["${var.SECURITY_GROUP}"]

  # the public SSH key
  key_name  = "${aws_key_pair.wpkey.key_name}"
  user_data = "${var.USER_DATA}"

  iam_instance_profile = "${aws_iam_instance_profile.s3-wpbucket-role-instanceprofile.name}"

  tags {
    Name         = "linux2ami-instance"
    Terraform    = "true"
    Environmnent = "${var.ENV}"
  }
}

output "instance_id" {
  value = "${aws_instance.instance.id}"
}

output "public_ip" {
  value = "${aws_instance.instance.public_ip}"
}

resource "aws_key_pair" "wpkey" {
  key_name   = "wpkey-${var.ENV}"
  public_key = "${file("${path.root}/${var.PATH_TO_PUBLIC_KEY}")}"
}
