# Security Groups are STATEFUL (meaning when you open port -> open both inbound and outbound) (NACL is stateless)
# Create inbound rules allow traffic in -> traffic automatically allowed back out
# Cannot block IP using SC (NACL instead)
# Specify allow rules, but NOT deny rules 
# All inbound traffic is blocked by default
resource "aws_security_group" "allow-ssh" {
  vpc_id      = "${aws_vpc.main.id}"
  name        = "allow-ssh"
  description = "security group that allows ssh and all egress traffic"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "allow-ssh"
  }
}
