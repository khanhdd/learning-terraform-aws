# Elastic Beanstalk (PaaS)

##  where you launch your app on without having to maintain the uderlying infrastructure

* Still responsible for EC2 instances, but AWS will provide you with updates you can apply
    * Updates can be appied manually or automatically
    * EC2 instance run Amazon Linux

## Features

* Handle application scaling for you

    * Load Balancer + Autoscaling group
    * Schedule scaling events or enable autoscaling based on a metric

* Similar to Heroku (only different is you still responsible for EC2 instances)

## Example 

