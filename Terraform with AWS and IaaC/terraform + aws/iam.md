#IAM

## Identity & Access Management - control access to AWS resources

* Groups
* Users (can have groups)
* Roles 

Roles can give users / services (temporary) access that they normally wouldn't have

Roles can be attached to EC2 instances

- From that instance, user or service can obtain access credentials
- Using access credentials, user/service can assume the role, which gives them permission to do something

## Example 

* User, group, pre-defined group's policy

```
# group definition
resource "aws_iam_group" "administrators" {
    name = "administrators"
}
# attach policy to group
resource "aws_iam_policy_attachment" "administrators-attach" {
    name = "administrators-attach"
    groups = ["${aws_iam_group.administrators.name}"]
    policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}
# user
resource "aws_iam_user" "admin1" {
    name = "admin1"
}
resource "aws_iam_user" "admin2" {
    name = "admin2"
}
# add user to group
resource "aws_iam_group_membership" "administrators-users" {
    name = "administrators-users"
    users = [
        "${aws_iam_user.admin1.name}",
        "${aws_iam_user.admin2.name}",
    ]
    group = "${aws_iam_group.administrators.name}"
}

output "warning" {
    value = "WARNING: make sure you're not using the AdministratorAccess policy for other users/groups/roles. If this is the case, don't run terraform destroy, but manually unlink the created resources"
}
```

* Roles, custom role's policy
```
# iam.tf
# create role
resource "aws_iam_role" "s3-mybucket-role" {
    name = "s3-mybucket-role"
    assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
# create role instance profile to attach to ec2 instance
resource "aws_iam_instance_profile" "s3-mybucket-role-instanceprofile" {
    name = "s3-mybucket-role"
    role = "${aws_iam_role.s3-mybucket-role.name}"
}
# create a custom role policy
resource "aws_iam_role_policy" "s3-mybucket-role-policy" {
    name = "s3-mybucket-role-policy"
    role = "${aws_iam_role.s3-mybucket-role.id}"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
              "s3:*"
            ],
            "Resource": [
              "arn:aws:s3:::mybucket-c29df1",
              "arn:aws:s3:::mybucket-c29df1/*"
            ]
        }
    ]
}
EOF
}
```
```
# instance.tf
# attach role to ec2 instance
# role:
  iam_instance_profile = "${aws_iam_instance_profile.s3-mybucket-role-instanceprofile.name}"
```

```
# s3.tf
# create s3 bucket
resource "aws_s3_bucket" "b" {
    bucket = "mybucket-c29df1"
    acl = "private"

    tags {
        Name = "mybucket-c29df1"
    }
}

```