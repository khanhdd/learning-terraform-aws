# Elastic Load Balancer

## Automatically distributes incoming traffic accoss multiple EC2 instances

* Scales when you receive more traffic 
* Healthcheck your instance
* Instance fail healthcheck -> no traffic sent to it
* New instance add by autoscaling group -> elb heathcheck

## ELB can also be used as SSL terminator

* It can offload the encryption away from the EC2 instances
* AWS can manage the SSL certificates for you

## Different type of Load Balancers

- Classic LB (ELB)
    * Routes traffic based on network information 
    * e.g. fw traffic from port 80 -> 8080 (internal app)
- App LB (ALB)

    * Routes traffic based on application level information
    * e.g. route /api, /website to different EC2 instances

## Example 

