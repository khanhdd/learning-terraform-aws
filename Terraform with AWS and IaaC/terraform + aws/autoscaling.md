# Autoscaling

## Automatically add/remove instances when certain threholds are reached

* e.g scaled out when more visitors

## Horizontal scaling vs Vertical scaling

* Horizontal scaling means that you scale by adding more machines into your pool of resources 
* Vertical scaling means that you scale by adding more power (CPU, RAM) to an existing machine

## Set up

- AWS launch configuration 

`ami-id, security-groups,...`
- Autoscaling group

`min-instances, max-instances, health-checks`

- Autoscaling policy

- CloudWatch alarm which wil trigger the autoscaling policy

- Optional: Alarm with AWS SNS, attach notify to auto scaling group

## Example 

