resource "aws_instance" "example" {
  ami           = "${lookup(var.AMIS, var.AWS_REGION)}"
  instance_type = "t2.micro"

  # the VPC subnet
  subnet_id = "${aws_subnet.main-public-1.id}"

  # the security group
  vpc_security_group_ids = ["${aws_security_group.allow-ssh.id}"]

  # the public SSH key
  key_name = "${aws_key_pair.mykey.key_name}"

  # ebs root volumes of your default AMI cannot be encrypted
  # "should" stop instance before taking snapshot of root device
  root_block_device {
    volume_size = 16
    volume_type = "gp2"

    # default is delete on termination, can tell AWS keep
    delete_on_termination = true
  }
}

# termination protection is turned off by default, must turn it on 
# can encrypted additional volumes
resource "aws_ebs_volume" "ebs-volume-1" {
  # always be the same az as the EC2 instance
  availability_zone = "ap-southeast-1a"
  size              = 8

  # gp2       Most Work Loads            General Purpose SSD      MaxIOPS/Volume 16000
  # io1       Databases                  Provisioned IOPS SSD     MaxIOPS/Volume 64000
  # st1       Big Data/Data Warehouses   Throughput Optimized HDD MaxIOPS/Volume 500
  # sc1       File Servers               Cold HDD                 MaxIOPS/Volume 250
  # Standard  Work load where data is IA EBS Magnetic             MaxIOPS/Volume 40-200
  type = "gp2"

  tags {
    Name = "extra volume data"
  }
}

# volumes exist on EBS, EBS like virtual hard disk
resource "aws_volume_attachment" "ebs-volume-1-attachment" {
  device_name = "/dev/xvdh"
  volume_id   = "${aws_ebs_volume.ebs-volume-1.id}"
  instance_id = "${aws_instance.example.id}"
}

# Snapshot exist on S3 (photograph of the disk - point in times copies of Volumes)
# Snapshot are incremental (only blocks has change since your last snap -> move to S3)
# Snapshot of encrypted volume are encrypted automatically
# Volume restored from encrypted snapshot are encrypted automatically
resource "aws_ebs_snapshot" "example_snapshot" {
  volume_id = "${aws_ebs_volume.example.id}"

  tags = {
    Name = "HelloWorld_snap"
  }
}

# Move EC2 Volume from one region -> another (one AZ -> another)
# 1. Take snapshot
# 2. Create AMI from snapshot
# 3. AMI copy to another region (not copy, just launch, skip step 4)
# 4. Launch instance in another region with AMI copy
resource "aws_ami_copy" "example" {
  name              = "terraform-example"
  description       = "A copy of ami-xxxxxxxx"
  source_ami_id     = "ami-xxxxxxxx"
  source_ami_region = "us-west-1"

  tags = {
    Name = "HelloWorld"
  }
}

# Create AMI's from both Volumes and Snapshots
# Create an AMI that will start a machine whose root device is backed by
# an EBS volume populated from a snapshot. It is assumed that such a snapshot
# already exists with the id "snap-xxxxxxxx".
# Restore an Amazon EBS volume with data from a snapshot stored in Amazon S3
resource "aws_ami" "example" {
  name                = "terraform-example"
  virtualization_type = "hvm"
  root_device_name    = "/dev/xvda"

  ebs_block_device {
    device_name = "/dev/xvda"
    snapshot_id = "snap-xxxxxxxx"
    volume_size = 8
  }
}

# Can share snapshot, only unencrypted.
# Share public or AWS account
resource "aws_ebs_snapshot_copy" "example_copy" {
  source_snapshot_id = "${aws_ebs_snapshot.example_snapshot.id}"
  source_region      = "us-west-2"

  tags = {
    Name = "HelloWorld_copy_snap"
  }
}

# How to encrypted root device volumes
# 1. Create snapshot of unencrypted root device volume 
# 2. Copy snapshot, encrypt it
# 3. AMI from encrypted snapshot
# 4. Launch instance from encrypted snapshot


# EBS vs Instance Store 
# Instance Store Volumes (Ephemeral Storage), root volumes will be deleted on termination
# IS cannot be stopped, if underlying host fails -> lose data
# EBS backed instance can stop, not lost data

