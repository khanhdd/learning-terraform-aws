# 4 Pricing Models: On Demand, Reserved, Spot, Delicated Host
# Note on Spot: instance terminated by AWS, you will not be charged for partial hour of usage. If terminate
# by yourself -> charged for any hour instance run  
resource "aws_instance" "example" {
  ami = "${lookup(var.AMIS, var.AWS_REGION)}"

  # EC2 Instance Types: FIGHT DR MCPXZAU (14)
  instance_type = "t2.micro"
}
