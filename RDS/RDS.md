# Relational Database (used for Online Transaction Processing - OLTP)

* MySQL
* MariaDB
* PostgreSQL
* SQL Server
* Aurora
* Oracle

### Essential
* RDS runs on virtual machines
* You cannot login to these os however
* Patching RDS OS/DB is Amz's responsibility
* RDS is not serverless
* Aurora Serverless is Serverless

---

### Backups
* Two different types of Backups for RDS:
* Automated Backups (in scheduled maintenance window)
* Database Snapshots (manually)

---

### Encryption At Rest (MySQL, Oracle, MS SQL, Postgre, Maria, Aurora)
* Done using AWS KMS
* RDS instance encrypted 
 -> data underlying, automated backups, read replicas, snapshots is encrypted


## Read Replicas
* Can be multi-AZ
* Used to increase performance
* Max up to 5 copies of any database
* Can have read replicas of read replicas (latency watch out!!)
* Must have backups turned on
* Can be in different regions
* Can be Aurora, MySQL, MariaDB, PostgreSQL
* Can be promoted to master, this will break the Read Replica
* Can have read replica in a second region
* Each read replica will have its own DNS end point

## Multi AZ 
* Used for Disaster Recovery (Automatic switch DNS endpoint when an AZ fail)
* You can force a failover from one AZ to another by rebooting the RDS instance
* For MySQL, SQL, Oracle, Postgre, MariaDB (Aurora have it own mechanism)


