resource "aws_key_pair" "rds_key" {
  key_name   = "rds_key"
  public_key = "${file("${var.PATH_TO_PUBLIC_KEY}")}"
}
