variable "AWS_REGION" {
  default = "ap-southeast-1"
}

variable "PATH_TO_PRIVATE_KEY" {
  default = "rds_key"
}

variable "PATH_TO_PUBLIC_KEY" {
  default = "rds_key.pub"
}

variable "AMIS" {
  type = "map"

  default = {
    ap-southeast-1 = "ami-01f7527546b557442"
  }
}

variable "RDS_PASSWORD" {}
