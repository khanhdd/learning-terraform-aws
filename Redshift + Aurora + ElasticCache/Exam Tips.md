# Redshift (used for Online Analytics Processing - OLAP)
* Used for business intelligence
* Available in only 1 AZ

## Backups
* Enable by default with a 1 day retention period
* Maximum retention period is 35 days
* Redshift always attempts to maintain at least three copies of your data (original, replica on compute nodes, backup in S3)
* Redshift can also asynchronously replicate your snapshots to S3 in another region for disaster recovery

# Elasticcache

* Memcached
* Redis

# Aurora
* 2 copies of data each AZ, min 3 AZ -> 6 copies of data
* Can share aurora snapshots with other AWS accounts
* 2 types of replicas available. Aurora and MySQL replicas. Automatic failover is only available with Aurora Replicas

